package com.vinicius.pos.services;

import java.math.BigDecimal;
import org.springframework.stereotype.Service;
import com.vinicius.pos.interfaces.IJurosCompostos;
import com.vinicius.pos.model.JurosCompostos;

@Service
public class JurosCompostosService implements IJurosCompostos{
	
	@Override public BigDecimal calcularMontante(JurosCompostos juros) {return juros.getCalcularMontateAplicacao();}
	@Override public BigDecimal calcularTempo(JurosCompostos juros) {return juros.getCalcularTempoAplicacao();}
	@Override public BigDecimal calcularTaxa(JurosCompostos juros) {return juros.getCalcularTaxaAplicacao();}
	@Override public BigDecimal calcularCapital(JurosCompostos juros) {return juros.getCalcularCapital();}
	
	

}
