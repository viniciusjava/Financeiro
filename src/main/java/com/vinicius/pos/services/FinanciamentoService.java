package com.vinicius.pos.services;

import java.math.BigDecimal;
import org.springframework.stereotype.Service;
import com.vinicius.pos.interfaces.IFinanciamento;
import com.vinicius.pos.model.Financiamento;

@Service
public class FinanciamentoService implements IFinanciamento{
	

	@Override public BigDecimal valorPrestacaoEntradaIgual(Financiamento fin) {return fin.calcularPrestacaoEntradaIguais();}
	@Override public BigDecimal valorPrestacaocomEntrada(Financiamento fin) {return fin.calculaPrestacaoComEntrada();}

}
