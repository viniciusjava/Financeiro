package com.vinicius.pos.resourses;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.vinicius.pos.interfaces.IFinanciamento;
import com.vinicius.pos.model.Financiamento;

@RestController
@RequestMapping("/financiamento")
public class FinanciamentoCtrl {
	
	@Autowired IFinanciamento serviceFinanciamento;
	
	
	@PostMapping ("/entrada")
	public ResponseEntity<BigDecimal> calculaPrestacaoComEntrada(@RequestBody Financiamento fin){
		return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest().build().toUri())
		.body(serviceFinanciamento.valorPrestacaocomEntrada(fin).setScale(2, BigDecimal.ROUND_HALF_UP));	
	}
	
	@PostMapping ("/iguais")
	public ResponseEntity<BigDecimal> calculaPrestacaoEntradaIguais(@RequestBody Financiamento fin){
		return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest().build().toUri())
		.body(serviceFinanciamento.valorPrestacaoEntradaIgual(fin).setScale(2, BigDecimal.ROUND_HALF_UP));	
	}
	
	

}
