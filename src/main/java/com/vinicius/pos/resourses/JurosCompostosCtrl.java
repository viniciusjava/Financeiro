package com.vinicius.pos.resourses;


import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.vinicius.pos.interfaces.IJurosCompostos;
import com.vinicius.pos.model.JurosCompostos;

@RestController
@RequestMapping("/juroscompostos")
public class JurosCompostosCtrl {
	
	@Autowired private IJurosCompostos serviceJuros;
	
	@PostMapping("/montante")
	public ResponseEntity<BigDecimal> calcularMontante(@RequestBody JurosCompostos juros){
		return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest().build().toUri())
				.body(serviceJuros.calcularMontante(juros).setScale(2, BigDecimal.ROUND_HALF_UP));	
	}
	
	@PostMapping("/tempo")
	public ResponseEntity<BigDecimal> calcularTempo(@RequestBody JurosCompostos juros){
		return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest().build().toUri())
				.body(serviceJuros.calcularTempo(juros).setScale(2, BigDecimal.ROUND_HALF_UP));
	}
	
	@PostMapping("/taxa")
	public ResponseEntity<BigDecimal> calcularTaxa(@RequestBody JurosCompostos juros){
		return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest().build().toUri())
				.body(serviceJuros.calcularTaxa(juros).setScale(2, BigDecimal.ROUND_HALF_UP));
	}
	
	@PostMapping("/capital")
	public ResponseEntity<BigDecimal> calcularCapital(@RequestBody JurosCompostos juros){
		return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest().build().toUri())
				.body(serviceJuros.calcularCapital(juros).setScale(2, BigDecimal.ROUND_HALF_UP));
	}

}
