package com.vinicius.pos.model;

import java.math.BigDecimal;
import org.springframework.stereotype.Component;


@Component
public class JurosCompostos {
	
	private Double taxa;
	private Double tempo;
	private Double capital;
	private Double montante;
	private Double juros;
	
	
	
	public Double getTaxa() {return taxa;}
	public void setTaxa(Double taxa) {this.taxa = taxa;}
	public Double getTempo() {return tempo;}
	public void setTempo(Double tempo) {this.tempo = tempo;}
	public Double getCapital() {return capital;}
	public void setCapital(Double capital) {this.capital = capital;}
	public Double getMontante() {return montante;}
	public void setMontante(Double montante) {this.montante = montante;}
	public Double getJuros() {return juros;}
	public void setJuros(Double juros) {this.juros = juros;}
	public JurosCompostos() {super();}
	
	
	
	public BigDecimal getCalcularMontateAplicacao() {return new BigDecimal(capital*getCalcularFatorJurosCompostos().doubleValue());}
	public BigDecimal getCalcularTempoAplicacao() {return new BigDecimal(Math.log(montante/capital)/Math.log(1+(taxa/100)));}
	public BigDecimal getCalcularTaxaAplicacao() {return new BigDecimal((Math.pow(montante/capital, 1/tempo))-1);}
	public BigDecimal getCalcularCapital() {return new BigDecimal(montante / getCalcularFatorJurosCompostos().doubleValue());}
	public BigDecimal getCalcularFatorJurosCompostos() {return new BigDecimal(Math.pow(1+(taxa/100), tempo));}
	

}
