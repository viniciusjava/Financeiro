package com.vinicius.pos.model;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

@Component
public class Financiamento {
	
	private Double taxa;
	private Double tempo;
	private BigDecimal prestacao;
	private Double valorBem;
	private Double entrada;
	
	
	public Double getEntrada() {return entrada;}
	public void setEntrada(Double entrada) {this.entrada = entrada;}
	public Double getTaxa() {return taxa;}
	public void setTaxa(Double taxa) {this.taxa = taxa;}
	public Double getTempo() {return tempo;}
	public void setTempo(Double tempo) {this.tempo = tempo;}
	public BigDecimal getPrestacao() {return prestacao;}
	public void setPrestacao(BigDecimal prestacao) {this.prestacao = prestacao;}
	public Double getValorBem() {return valorBem;}
	public void setValorBem(Double valorBem) {this.valorBem = valorBem;}
	public Financiamento() {super();}
	
	public BigDecimal coeficienteFinanciamento() {
		return new BigDecimal((taxa/100)/(1-(1/calcularFatorJurosCompostos().doubleValue())));
	}
	
	public BigDecimal calcularFatorJurosCompostos() {
		return new BigDecimal(Math.pow(1+(taxa/100), tempo));
	}
	
	public BigDecimal calcularPrestacaoEntradaIguais(){
		return new BigDecimal((coeficienteFinanciamento().doubleValue()*valorBem) / (1+coeficienteFinanciamento().doubleValue()));
	}
	
	public BigDecimal calculaPrestacaoComEntrada() {
		return new BigDecimal((valorBem - entrada)*coeficienteFinanciamento().doubleValue());
	}
	

}
