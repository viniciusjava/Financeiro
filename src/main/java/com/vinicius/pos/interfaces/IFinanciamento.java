package com.vinicius.pos.interfaces;

import java.math.BigDecimal;

import com.vinicius.pos.model.Financiamento;

public interface IFinanciamento {
	
	BigDecimal valorPrestacaoEntradaIgual(Financiamento fin);
	BigDecimal valorPrestacaocomEntrada(Financiamento fin);

}
